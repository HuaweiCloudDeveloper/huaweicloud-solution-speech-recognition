# -*- coding:utf-8 -*-

from huaweicloudsdkcore.auth.credentials import BasicCredentials
from huaweicloudsdksis.v1.region.sis_region import SisRegion
from huaweicloudsdkcore.exceptions import exceptions
from huaweicloudsdksis.v1 import *
from obs import ObsClient
import traceback
import os
from urllib.parse import unquote_plus


def check_configuration(context):
    result_bucket = context.getUserData('result_bucket')
    if not result_bucket:
        return 'result_bucket is not configured'
    ak = context.getAccessKey().strip()
    sk = context.getSecretKey().strip()
    if not ak or not sk:
        ak = context.getUserData('ak', '').strip()
        sk = context.getUserData('sk', '').strip()
        if not ak or not sk:
            return 'ak or sk is empty'


# 函数工作流调用入口
def handler(event, context):
    log = context.getLogger()
    result = check_configuration(context)
    if result is not None:
        return result

    records = event.get("Records", None)
    if records is None:
        return 'Records is empty'

    processor = Processor(context)
    try:
        for record in records:
            processor.process(record)
    except Exception as e:
        log.error("failed to process obs, "
                  f"exception：{traceback.format_exc(e)}")
    finally:
        # 释放资源
        processor.obs_client.close()
    return 'Done'


class Processor:
    def __init__(self, context=None):
        self.log = context.getLogger()
        self.obs_client = new_obs_client(context)
        self.sis_client = new_sis_client(context)
        self.obs_bucket_name = None
        self.obs_object_key = None
        self.output_object_key = None
        self.result_bucket = context.getUserData('result_bucket')

    def process(self, record):
        # 解析record
        self.analysis_record(record)
        # 语音识别
        response = self.recognize_flash_asr()
        if response is None:
            return
        # 返回的内容写入与图片同名的json文件中，然后上传到指定的输出桶
        self.upload_result(response)

    def recognize_flash_asr(self):
        client = self.sis_client
        # 获取识别结果
        try:
            request = RecognizeFlashAsrRequest()
            request._property = "chinese_16k_conversation"
            request.audio_format = "wav"
            request.add_punc = "yes"
            request.obs_bucket_name = self.obs_bucket_name
            request.obs_object_key = self.obs_object_key
            response = client.recognize_flash_asr(request)
            return response
        except exceptions.ClientRequestException as e:
            self.log.error(f"failed to RecognizeFlashAsr："
                           f"status_code：{e.status_code}, "
                           f"request_id:{e.request_id}, "
                           f"error_code:{e.error_code}. "
                           f"error_msg:{e.error_msg}")
            return "Failed to invoke the SIS service"


    def upload_result(self, result):
        try:
            resp = self.obs_client.putContent(self.result_bucket,
                                              self.output_object_key,
                                              content=result)
            if resp.status < 300:
                self.log.info("succeed to upload result.")
            else:
                self.log.error("failed to upload result, "
                               f"requestId：{resp.requestId} "
                               f"errorCode：{resp.errorCode} "
                               f"errorMessage：{resp.errorMessage}")
        except Exception as e:
            self.log.error("failed to upload result, "
                           f"exception：{traceback.format_exc(e)}")


    def analysis_record(self, record):
        region = get_region(record)
        # 提取语音文件所在的obs桶和key
        (bucket_name, object_key) = get_obs_obj_info(record)
        self.obs_bucket_name = bucket_name
        self.log.info("input bucket_name: %s", bucket_name)
        self.obs_object_key = unquote_plus(object_key)
        self.log.info("input object: %s", self.obs_object_key)
        # 拼接语音文件obs访问url
        self.obs_url = "https://" + bucket_name + ".obs." + \
                    region + ".myhuaweicloud.com/" + self.obs_object_key
        (path, filename) = os.path.split(self.obs_object_key)
        (filename, _) = os.path.splitext(filename)
        # 处理结果输出对象
        self.output_object_key = path + filename + ".json"


def get_obs_obj_info(record):
    if 's3' in record:
        s3 = record['s3']
        return (s3['bucket']['name'], s3['object']['key'])
    else:
        obs_info = record['obs']
        return (obs_info['bucket']['name'], obs_info['object']['key'])


def new_obs_client(context):
    return ObsClient(
        access_key_id=context.getAccessKey(),
        secret_access_key=context.getSecretKey(),
        server="obs.cn-north-4.myhuaweicloud.com",
    )


def new_sis_client(context):
    ak = context.getAccessKey().strip()
    sk = context.getSecretKey().strip()
    credentials = BasicCredentials(ak, sk)
    client = SisClient.new_builder() \
        .with_credentials(credentials) \
        .with_region(SisRegion.value_of("cn-north-4")) \
        .build()
    return client


def get_region(record):
    if 'eventRegion' in record:
        return record.get("eventRegion", "cn-north-4")
    else:
        return record.get("awsRegion", "cn-north-4")