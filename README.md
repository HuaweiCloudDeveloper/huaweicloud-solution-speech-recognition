[TOC]

**解决方案介绍**
===============
该解决方案基于华为云语音交互服务语音识别构建，可自动将用户上传到对象存储服务的wav语音文件转化为文字，并将结果存放到指定OBS桶。该方案可以将用户上传在OBS的语音文件识别成可编辑的文本，支持中文普通话的识别和合成，其中语音识别还支持带方言口音的普通话识别以及方言（四川话、粤语和上海话）的识别。适用于如下场景：识别客服、客户的语音，进一步通过文本检索，检查有没有违规、敏感词、电话号码等信息。对会议记录的音频文件，进行快速的识别，转化成文字，方便进行会议记录等场景。

解决方案实践详情页面地址：https://www.huaweicloud.com/solution/implementations/speech-recognition.html

**架构图**
---------------
![架构图](./document/speech-recognition.png)

**架构描述**
---------------
该解决方案将会部署如下资源：

1.创建2个对象存储服务OBS桶，一个用于用户上传和存储用户的语音文件；另一个用于存储语音识别结果，用户可以从该桶中获取结果并处理。


2.函数工作流FunctionGraph，只需编写业务函数代码并设置运行的条件，即可以弹性、免运维、高可靠的方式运行。

3.语音识别服务，将wav语音文件转化为文字。

**组织结构**
---------------

``` lua
huaweicloud-solution-speech-recognition
├── speech-recognition.tf.json -- 资源编排模板
├── functiongrap
    ├── speech_recognition.py -- 函数文件
```
**开始使用**
---------------
1.登录华为云[对象存储服务](https://console.huaweicloud.com/console/?region=cn-north-4&locale=zh-cn#/obs/manager/buckets)控制台，查看自动创建的OBS桶列表。

图1 对象存储服务控制台
![对象存储服务控制台](./document/readme-image-001.png)

2.选择用于上传和存储语音文件的桶“wwwy-1”（实际桶名称以部署指定参数为准），上传wav语音文件。

图2 上传语音文件
![上传语音文件](./document/readme-image-002.png)

3.选择用于存放结果的“wwwy-2”桶（实际桶名称以部署指定参数为准），即可自动获取该语音文件的识别结果，以语音文件名称为前缀的JSON文件存储。

图3 获取识别结果文件
![获取识别结果文件](./document/readme-image-003.png)